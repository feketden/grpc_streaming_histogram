#include "histogram.grpc.pb.h"

#include <grpcpp/grpcpp.h>

#include <string>

#include <unordered_map>

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using grpc::ServerReaderWriter;

using histogram::getHistogramResponse;
using histogram::getHistogramRequest;
using histogram::Histogram;

// Server Implementation
class CHistogramService final : public Histogram::Service
{
	Status getHistogram(
			ServerContext* p_context,
			ServerReaderWriter<getHistogramResponse, getHistogramRequest>* p_stream) override
	{
		std::unordered_map<std::string, int> result;
		getHistogramRequest sr;
		while (p_stream->Read(&sr))
		{
//			if (result.countains(sr.word))
			if (result.find(sr.word()) != result.end())
				++result[sr.word()];
			else
				result[sr.word()] = 1;
		}

		getHistogramResponse response;
		// write all values to the stream
//		for (const auto& [word, count] : result)
		for (auto it = result.begin(); it != result.end(); it++)
		{
			response.set_word(it->first);
			response.set_count(it->second);
			p_stream->Write(response);
			// possible delay
		}

		return Status::OK;
	}
};

void RunServer()
{
  std::string server_address("0.0.0.0:50052");
  CHistogramService service;

  ServerBuilder builder;
  // Listen on the given address without any authentication mechanism
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  // Register "service" as the instance through which
  // communication with client takes place
  builder.RegisterService(&service);

  // Assembling the server
  std::unique_ptr<Server> server(builder.BuildAndStart());
  std::cout << "Server listening on port: " << server_address << std::endl;

  server->Wait();
}

int main(int argc, char** argv)
{
  RunServer();
  return 0;
}

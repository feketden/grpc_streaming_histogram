#include "histogram.grpc.pb.h"

#include <grpcpp/grpcpp.h>

#include <exception>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::ClientReaderWriter;

using histogram::getHistogramRequest;
using histogram::getHistogramResponse;
using histogram::Histogram;

struct SResponse
{
	SResponse(int p_count, const std::string& p_word)
		: m_count(p_count)
		, m_word(p_word)
	{}
	int m_count;
	std::string m_word;
};

class CRequestError : public std::exception
{
	public:
		CRequestError(const std::string& p_what)
			: m_what(p_what)
		{}

		const char* what() const noexcept override
		{ return m_what.c_str(); }

	private:
		const std::string m_what;
};

class CHistogramClient
{
	public:
		CHistogramClient(std::shared_ptr<Channel> p_channel)
			: m_stub(Histogram::NewStub(p_channel))
		{}

		// Assembles client payload, sends it to the server, and returns its response
		std::vector<SResponse> SendRequest(const std::string& p_filename)
		{
			ClientContext context;
//			std::shared_ptr<ClientReaderWriter<getHistogramRequest, getHistogramResponse>> stream(m_stub->getHistogram(&context));
			auto stream = m_stub->getHistogram(&context);

			std::ifstream inputFile(p_filename);
			if (!inputFile.is_open())
			{
				std::stringstream errorMessage;
				errorMessage << "Could not open the file - '" << p_filename << "'";
				throw CRequestError(errorMessage.str());
			}

			std::string line;
			std::string word;
			getHistogramRequest request;

			std::cout << "Input file content: " << std::endl;
			while (getline(inputFile, line))
			{
				std::cout << line << std::endl;
				std::istringstream ss(line);

				while (ss >> word)
				{
					request.set_word(word);
					stream->Write(request);
					// possible delay
				}
			}

			stream->WritesDone();

			std::vector<SResponse> response;
			getHistogramResponse serverResponse;
			while (stream->Read(&serverResponse))
			{
				response.push_back(SResponse(serverResponse.count(), serverResponse.word()));
			}

			Status status = stream->Finish();
			if (status.ok() == true)
				return response;
			else
				throw CRequestError("Response status from the service was not okay!");

			return {};
		}

	private:
		std::unique_ptr<Histogram::Stub> m_stub;
};

void RunClient(const std::string& p_filename)
{
	std::string target_address("0.0.0.0:50052");
	// Instantiates the client
	CHistogramClient client(
			// Channel from which RPCs are made - endpoint is the target_address
			grpc::CreateChannel(target_address,
			// Indicate when channel is not authenticated
			grpc::InsecureChannelCredentials()));

	try
	{
		const auto response = client.SendRequest(p_filename);

		std::cout << "Everything went well!" << std::endl;
		std::cout << "Response from service: " << std::endl;
		// Prints results
		for (const auto& elem : response)
			std::cout << "Word: " << elem.m_word << " Count: " << elem.m_count << std::endl;
	}
	catch (const CRequestError& p_ex)
	{
		std::cout << "An error was caught: " << p_ex.what() << std::endl;
	}
}

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		std::cout << "There is not appropriate number of input parameters! " <<
			"Only the input filename!" << std::endl;
		return 0;
	}

	RunClient(argv[1]);
	return 0;
}
